<?php

namespace Hkw\WangEditor;

use Dcat\Admin\Extend\ServiceProvider;
use Dcat\Admin\Admin;
use Dcat\Admin\Form;
use Hkw\WangEditor\Form\WangEditor;

class WangEditorServiceProvider extends ServiceProvider
{

    public function register()
    {
        //
    }

    public function init()
    {
        parent::init();
        $this->loadViewsFrom(__DIR__ . '/../resources/views' , 'hkw-form-wangeditor');
        $this->loadRoutesFrom(__DIR__ . '/Http/routes.php');
        Admin::booting(function () {
            Form::extend('wang' , WangEditor::class);
        });
    }
}
