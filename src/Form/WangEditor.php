<?php

namespace Hkw\WangEditor\Form;

use Dcat\Admin\Admin;
use Dcat\Admin\Form\Field;
use Helium\Sts\Facades\AliYunSTS;
use Helium\Sts\Sts;
use Qiniu\Auth;
use Qiniu\Config;
use Storage;

class WangEditor extends Field
{
    protected        $view = 'hkw-form-wangeditor::index';
    protected static $css  = [
        '@extension/hkw/wang-editor/v5/style.css' ,
        '@extension/hkw/wang-editor/v5/custom.css' ,
    ];
    protected static $js   = [
        '@extension/hkw/wang-editor/v5/index.js' ,
    ];

    protected $height = 500;
    protected $zIndex = 0;

    private function imgShow($path , $thumb = false)
    {
        if (empty($path)) {
            return '';
        }
        if (strpos($path , 'http://') !== false || strpos($path , 'https://') !== false) {
            return $this->addHttps($path);
        }
        if ($thumb) {
            return $this->addHttps(asset('storage' , config('admin.https')) . '/' . pathinfo($path)['dirname'] . '/thumb_' . pathinfo($path)['basename']);
        } else {
            return $this->addHttps(Storage::url($path));
        }
    }

    private function addHttps($url)
    {
        if ( ! request()->isSecure()) {
            return $url;
        }
        if (strpos($url , 'http://') !== false) {
            return str_replace('http://' , 'https://' , $url);
        }
        return $url;
    }

    public function height($height = 500)
    {
        $this->height = $height;
        return $this;
    }

    public function zIndex($zIndex = 0)
    {
        $this->zIndex = $zIndex;
        return $this;
    }

    public function render()
    {
        config('filesystems.default') == 'qiniu' && $this->qiniuConfig();
        config('filesystems.default') == 'oss' && $this->ossConfig();
        $this->addVariables(['height' => $this->height , 'zIndex' => $this->zIndex]);
        return parent::render();
    }


    private function qiniuConfig()
    {
        Admin::js('@extension/hkw/wang-editor/qiniu.min.js');
        $config           = new Config();
        $config->useHTTPS = 'https' == request()->getScheme();
        $saveKey          = config('admin.upload.directory.image') . '/$(etag)$(ext)';
        $policy           = [
            'saveKey'    => $saveKey ,
            'returnBody' => json_encode([
                'status' => true ,
                'data'   => [
                    'id' => $saveKey , //不想要完整 url 的话改成 $saveKey 即可
                ] ,
            ]) ,
        ];
        $url              = $config->getUpHost(
            config('filesystems.disks.qiniu.access_key') ,
            config('filesystems.disks.qiniu.bucket')
        );
        $pos_start        = strpos($url , '-');
        $pos_end          = strpos($url , '.');
        $config           = [
            'region'     => 'qiniu.region.' . substr($url , $pos_start + 1 , $pos_end - $pos_start - 1) ,
            'upprotocol' => request()->isSecure() ? 'https' : 'http' ,
            'chunkSize'  => 1
        ];
        $auth             = new Auth(config('filesystems.disks.qiniu.access_key') , config('filesystems.disks.qiniu.secret_key'));
        $this->addVariables(['uptoken' => $auth->uploadToken(config('filesystems.disks.qiniu.bucket') , null , 3600 , $policy) , 'config' => $config]);
    }

    private function ossConfig()
    {
        /*$config = [
            'access_key_id'     => config('filesystems.disks.oss.access_key') ,
            'access_key_secret' => config('filesystems.disks.oss.secret_key') ,
            'bucket_name'       => config('filesystems.disks.oss.bucket') ,
            'endpoint'          => config('filesystems.disks.oss.endpoint') ,
            'role_arn'          => 'acs:ram::1270844568592778:role/ramosshisums' ,
            'policy'            => [
                "Statement" => [
                    [
                        "Action"   => [
                            "oss:GetObject" ,
                            "oss:PutObject" ,
                            "oss:DeleteObject" ,
                            "oss:ListParts" ,
                            "oss:AbortMultipartUpload" ,
                            "oss:ListObjects"
                        ] ,
                        "Effect"   => "Allow" ,
                        "Resource" => [
                            "acs:oss:*:*:" . config('filesystems.disks.oss.bucket') ,
                            "acs:oss:*:*:" . config('filesystems.disks.oss.bucket') . "/*"
                        ]
                    ]
                ] ,
                "Version"   => "1"
            ] ,
            'token_expire_time' => 900
        ];*/
        Admin::js('@extension/hkw/wang-editor/aliyun-oss-sdk-6.16.0.min.js');
        //$this->addVariables((new Sts($config))->token());
        $this->addVariables(['region' => str_replace('.aliyuncs.com' , '' , config('filesystems.disks.oss.endpoint'))]);
    }
}
