<?php

namespace Hkw\WangEditor;

use Dcat\Admin\Extend\Setting as Form;

class Setting extends Form
{
    public function title()
    {
        return '配置默认参数';
    }

    public function form()
    {
        $this->number('default_height', '默认高度')->default(400)->min(100);
        $this->number('default_z_index', '默认zIndex')->default(10000)->min(0);
    }
}
