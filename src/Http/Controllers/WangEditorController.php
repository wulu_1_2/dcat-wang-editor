<?php

namespace Hkw\WangEditor\Http\Controllers;

use Dcat\Admin\Layout\Content;
use Dcat\Admin\Admin;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class WangEditorController extends Controller
{
    public function index(Content $content)
    {
        return $content
            ->title('Title')
            ->description('Description')
            ->body(Admin::view('hkw.wang-editor::index'));
    }

    public function editorImage(Request $request)
    {
        try {
            $path = $request->file('file')->store(config('admin.upload.directory.image'));
            return response()->json([
                'errno' => 0 ,
                'data'  => [
                    'url'  => imgShow($path) ,
                    'alt'  => basename($path) ,
                    'href' => 'javascript:;'
                ]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'errno'   => 1 ,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function editorVideo(Request $request)
    {
        try {
            $path = $request->file('file')->store(config('admin.upload.directory.file'));
            return response()->json([
                'errno' => 0 ,
                'data'  => [
                    'url' => imgShow($path) ,
                ]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'errno'   => 1 ,
                'message' => $e->getMessage()
            ]);
        }
    }
}